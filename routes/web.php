<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function(){
    Route::get('/home', 'HomeController@index');
    Route::get('/admin/products', 'ProductController@index');
});

Route::group(['middleware' => ['auth', 'newmiddleware']], function () {
    Route::get('/admin/products/creates', 'ProductController@create')->name('sozdat');
    Route::post('/admin/products/store', 'ProductController@store');
    Route::post('/admin/products/update/{id}', 'ProductController@update');
    Route::get('/admin/products/edit/{id}', 'ProductController@edit');
    Route::get('/admin/products/destroy/{id}', 'ProductController@destroy');
    Route::post('/s', 'ProductController@search');
});
