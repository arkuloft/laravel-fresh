@extends('layouts.app')

@section('content')
    <div>
        @foreach($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
    </div>

    <form action="/admin/products/store" method="POST">
        {{csrf_field()}}
        <input type="text" name="name" value="{{old('name')}}">
        <input type="text" name="price" value="{{old('price')}}">
        <input type="submit">
    </form>
@endsection