@extends('layouts.app')



@section('content')
    <a href="{{action('\App\Http\Controllers\ProductController@create')}}">
    <a href="{{route('sozdat')}}">
    <button class="btn">Создать товар</button>
    </a>
    <table>
        <tr>
            <td>Имя</td>
            <td>Цена</td>
            <td>Управление</td>
        </tr>
        @foreach($products as $product)
            <tr>
                <td>
                    <a href="/admin/products/edit/{{$product->id}}">
                        {{$product->name}}
                    </a>
                </td>
                <td>{{$product->price}}</td>
                <td><a href="/admin/products/destroy/{{$product->id}}">Удалить</a></td>
            </tr>
        @endforeach
    </table>

    {{$products->render()}}
@endsection