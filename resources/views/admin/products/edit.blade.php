@extends('layouts.app')

@section('content')
    <div>
        @foreach($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
    </div>
    <form action="/admin/products/update/{{$product->id}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="text" name="name" value="{{$product->name}}">
        <input type="file" name="image">
        <input type="text" name="price" value="{{$product->price}}">
        <input type="submit">
    </form>
@endsection