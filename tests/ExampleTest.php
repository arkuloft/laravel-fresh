<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
             ->see('Laravel');
    }

    public function testRegistration()
    {
        $this->visit('/register')
            ->type('Vasya', 'name')
            ->type(rand(1,1000).'asd@asdd.ru', 'email')
            ->type('superpassword', 'password')
            ->type('superpassword', 'password_confirmation')
            ->press('Register')
            ->seePageIs('/home');
    }
}
