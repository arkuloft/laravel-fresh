<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::paginate(3);
        $data['products'] = $products;
        return view('admin.products.index', $data);
    }

    public function create()
    {
        return view('admin.products.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'price' => 'integer',
            'name'  => 'required|min:5',
        ]);
        $product = new Product();
        $product->name = $request->input('name');
        $product->price = $request->price;
        $product->category_id = 1;
        $product->save();
        $data['product'] = $product;

        $email = "asd@asd.ru";

        Mail::send('emails.welcome', $data, function($message) use ($email)
        {
            $message->to($email, 'Получателю')
                ->cc('asd@asd.ru')
                ->bcc('asd@asd.ru')
                ->attach('favicon.ico')
                ->subject('Привет из письма!');
        });

        return redirect('/admin/products');
    }


    public function edit($id)
    {
        $product = Product::find($id);
        $data['product'] = $product;
        return view('admin.products.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'price' => 'integer',
           'name'  => 'required|min:5',
           'image' => 'image|max:10'
        ]);
        if ($request->file('image')) {
            $request->file('image')
                ->move('uploads', '1.jpg');
        }
        $product = Product::find($id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->save();

        return back();

    }

    public function destroy($id)
    {
        Product::destroy($id);
        return back();
    }

    public function search(Request $request)
    {
        $products = Product::search($request->s);
        $data['products'] = $products;

        return view('admin.products.index', $data);
    }
}
