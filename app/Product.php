<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    public static function search($string)
    {
        return Product::where('name', 'LIKE', "%$string%")->get();
    }
}
